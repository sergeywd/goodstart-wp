// Imports
var {gulp, src, dest, watch, series, parallel} = require('gulp');
var touch        = require('gulp-touch-fd');
var sass         = require('gulp-sass');
var sassGlob     = require('gulp-sass-glob');
var postcss      = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var cssnano      = require("cssnano");
var gulpif       = require('gulp-if');
var sourcemaps   = require('gulp-sourcemaps');
var include      = require("gulp-include");
var uglify       = require('gulp-uglify-es').default;
var eslint       = require('gulp-eslint');
var cache        = require('gulp-cache');
var imagemin     = require('gulp-imagemin');
var browserSync  = require('browser-sync').create();
var fs           = require('fs');
var htmlInjector = require("bs-html-injector");
var twig         = require('gulp-twig');
var tailwindcss  = require('tailwindcss');
var purgecss     = require('@fullhuman/postcss-purgecss');
var prefixWrap   = require("postcss-prefixwrap");
var babel        = require('gulp-babel');

var whitelistClasses = ['text-white', 'text-color-text', 'justify-center', 'justify-start', 'justify-end', 'text-left', 'text-center', 'text-right', 'font-black', 'font-medium', 'font-semibold', 'font-normal', 'font-bold'];

// Setup enviroment type (Static, Wordpress)
try {
  var options = require('./options.js');
  // Local enviroment setup override
  var isStatic       = options.isStatic;
  var isWP           = options.isWP;
  var templateNameWP = options.templateNameWP;
  var isDevMode      = true;
} catch(err) {
  // Default enviroment setup (in most cases for deployment)
  var isStatic       = false;  // Static website
  var isWP           = true;   // WordPress Project
  var templateNameWP = 'theme-dir-name';
}

// Local server
var server = function(done) {
  // Run server
  if(isStatic) {
    browserSync.use(htmlInjector, {
        files: "./static/*.html"
    });
    browserSync.init({
      server: {
        baseDir: "./static"
      }
    });
  }
  else {
    browserSync.use(htmlInjector, {
        files: "wordpress/**/*.php"
    });
    browserSync.init({
      proxy: options.serverLink
    });
  }

  // Signal completion
	done();
}

// Reload the browser when files change
var reloadBrowser = function (done) {
  console.log("Reloading Preview.\n");
	browserSync.reload();
	done();
}

// Compile main Sass file
var stylesTailwind = function(done) {
  return src(['src-assets/sass/tailwind-base.sass', 'src-assets/sass/tailwind-utilities.sass'])
    .pipe(gulpif(isDevMode, sourcemaps.init({ loadMaps: true })))
    .pipe(sassGlob())
    .pipe(sass({ outputStyle: 'expanded' }))
    .on("error", sass.logError)
    .pipe(gulpif(isDevMode,
      // On Dev
      postcss([
        tailwindcss('./tailwind.config.js'),
        require('postcss-viewport-height-correction'),
      ]),
      // On Production
      postcss([
        tailwindcss('./tailwind.config.js'),
        purgecss({
          content: ['static/**/*.html','static/**/.*js','wordpress/themes/' + templateNameWP + '/**/*.php', 'wordpress/themes/' + templateNameWP + '/**/*.html', 'src-assets/js/**/*.js'],
          whitelist: whitelistClasses,
          defaultExtractor: content => content.match(/[\w-/.:]+(?<!:)/g) || []
        }),
        autoprefixer(),
        require('postcss-viewport-height-correction'),
        cssnano()
      ]),
    ))
    .pipe(gulpif(isDevMode, sourcemaps.write('.') ))
    .pipe(gulpif(isStatic,dest('./static/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch())
    .pipe(browserSync.stream({ match: '**/*.css' }));
}

var stylesMain = function(done) {
  return src(['src-assets/sass/main.sass'])
    .pipe(gulpif(isDevMode, sourcemaps.init({ loadMaps: true })))
    .pipe(sassGlob())
    .pipe(sass({ outputStyle: 'expanded' }))
    .on("error", sass.logError)
    .pipe(gulpif(isDevMode,
      // On Dev
      postcss([
        tailwindcss('./tailwind.config.js'),
      ]),
      // On Production
      postcss([
        tailwindcss('./tailwind.config.js'),
        autoprefixer(),
        require('postcss-viewport-height-correction'),
        cssnano()
      ]),
    ))
    .pipe(gulpif(isDevMode, sourcemaps.write('.') ))
    .pipe(gulpif(isStatic,dest('./static/assets/css')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch())
    .pipe(browserSync.stream({ match: '**/*.css' }));
}

var stylesAdminACFBlocks = function(done) {
  return src(['src-assets/sass/admin-acf-blocks.sass'])
    .pipe(sassGlob())
    .pipe(sass()).on("error", sass.logError)
    .pipe(postcss([
      tailwindcss('./tailwind.config.js'),
      purgecss({
        content: ['static/**/*.html','static/**/.*js','wordpress/themes/' + templateNameWP + '/**/*.php', 'wordpress/themes/' + templateNameWP + '/**/*.html'],
        whitelist: whitelistClasses,
        defaultExtractor: content => content.match(/[\w-/.:]+(?<!:)/g) || []
      }),
      prefixWrap(".acf-block-preview"),
      autoprefixer(),
      cssnano()
    ]))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch());
}

var stylesAdminGlobal = function(done) {
  return src(['src-assets/sass/admin-global.sass', 'src-assets/sass/editor-styles.sass'])
    .pipe(sassGlob())
    .pipe(sass()).on("error", sass.logError)
    .pipe(postcss([
      autoprefixer(),
      cssnano()
    ]))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/css')))
    .pipe(touch());
}

// Scripts tasks
var scriptsVendor = function() {
  return src('src-assets/js/vendor/*.js')
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(include())
        .on('error', console.log)
      .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulpif(isStatic,dest('./static/assets/js/vendor')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js/vendor')))
    .pipe(touch());
}

// Check main JS file syntax & minify it
var scriptMain = function(done) {
  return src('src-assets/js/main.js')
    .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(include())
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
    .pipe(sourcemaps.write('../js'))
    .pipe(gulpif(isStatic,dest('./static/assets/js')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/js')))
    .pipe(touch());
}

// Copy fonts to appropriate folder
var fonts = function(done) {
  return src(['src-assets/fonts/**/*.*', 'node_modules/@fortawesome/fontawesome-free/webfonts/**/*.*'])
    .pipe(gulpif(isStatic,dest('./static/assets/fonts')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/fonts')));
}

// Optimize all images
var images = function(done) {
  return src('src-assets/img/**/*')
  .pipe(cache(imagemin([
    // svg
    imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { cleanupIDs: true }
        ]
    }),
    ])))
    .pipe(gulpif(isStatic,dest('./static/assets/img')))
    .pipe(gulpif(isWP,dest('./wordpress/themes/' + templateNameWP + '/assets/img')));
}

// Watch for changes
var watchSource = function (done) {
  watch(['src-assets/sass/**/*.(sass|scss)'], series(stylesMain));
  watch(['src-assets/sass/tailwind-base.sass', 'src-assets/sass/tailwind-utilities.sass'], series(stylesTailwind));
  watch('tailwind.config.js', series(stylesTailwind, stylesMain));
  watch('src-assets/sass/admin-acf-blocks.sass', series(stylesAdminACFBlocks));
  watch(['src-assets/sass/admin-global.sass', 'src-assets/sass/editor-styles.sass'], series(stylesAdminGlobal));
  watch('./static/*.html', series(htmlInjector));
  watch('src-assets/js/vendor/*.js', series(scriptsVendor, reloadBrowser));
  watch('src-assets/js/main.js', series(scriptMain, reloadBrowser));
  watch('src-assets/fonts/**/*.*', series(fonts, reloadBrowser));
  watch('src-assets/img/**/*.+(png|jpg|jpeg|gif|svg)', series(images));

	// done();
};

// Basic tasks
exports.basic = series(
  parallel(
    stylesMain,
    stylesTailwind,
    stylesAdminACFBlocks,
    stylesAdminGlobal,
    scriptsVendor,
    scriptMain,
    fonts,
    images
  )
);

// Tasks for compiling project on CI/CD
exports.build = series(
  parallel(
    stylesMain,
    stylesTailwind,
    stylesAdminACFBlocks,
    stylesAdminGlobal,
    scriptsVendor,
    scriptMain,
    fonts,
    images
  )
);
// Alternative name for the deployment task group
exports.deploy = series(
  exports.build
);

// Basic + Browser Sync + Watch
// Development tasks
exports.default = series(
  exports.basic,
  server,
  watchSource
);
